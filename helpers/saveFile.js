const fs = require('fs');

const filePath = './database/data.json';

const index = () => {
    if(!fs.existsSync( filePath )) {
        return null;
    }
    const data = fs.readFileSync( filePath, { encoding: 'utf-8' } );
    return JSON.parse( data );
}

const store = ( data ) => {
    fs.writeFileSync( filePath, JSON.stringify( data ) );
}

module.exports = {
    index,
    store
}