const { v4: uuidv4 } = require('uuid');

class Task {

    id = '';
    desc = '';
    CompletedAt = null;

    constructor( desc ) {

        this.id = uuidv4();
        this.desc = desc;
        this.CompletedAt = null;

    }
}

module.exports = Task;