const Task = require('./task');

class Tasks {

    _lists = {};

    constructor() {
        this._lists = {};
    }

    get listArr() {
        const list = [];
        Object.keys( this._lists ).forEach( key => {
            const task = this._lists[key];
            list.push( task );
        })
        return list;
    }

    insertTasksFronArray(  tasks = [] ) {
        tasks.forEach( task => {
            this._lists[task.id] = task;
        });
    }

    createTask = ( desc = '' ) => {
        const task = new Task( desc );
        this._lists[task.id] = task;
    }

    completeList() {
        console.log('');
        this.listArr.forEach( (task, i) => {
            const idx = `${i + 1}`.green;
            let status = (task.CompletedAt !== null) ? 'Completed'.green : 'Pending'.red;
            console.log(`${idx}. ${ task.desc } :: ${ status }`);
            
        });
    }

    completedTasks( completed = true ) {
        console.log('');
        let count = 0;
        this.listArr.forEach( task => {
            const { desc, CompletedAt } = task;
            let status = ( CompletedAt ) ? 'Completed'.green : 'Pending'.red;
            if ( completed ) {
                if ( CompletedAt ) {
                    count += 1;
                    console.log(`${ (count + '.').green } ${ desc } :: ${ CompletedAt }`);
                }
            } else {
                if ( !CompletedAt ) {
                    count += 1;
                    console.log(`${ (count + '.').green } ${ desc } :: ${ status }`);
                }
            }
        });
    }

    deleteTask(id = '') {
        if ( this._lists[id] ) {
            delete this._lists[id];
        }
    }

    toggleCompleted( ids = [] ) {
        
        ids.forEach( id => {
            const task = this._lists[id];
            if ( !task.CompletedAt ) {
                task.CompletedAt = new Date().toISOString();
            }
        });

        this.listArr.forEach( task => {
            if ( !ids.includes( task.id ) ) {
                this._lists[ task.id ].CompletedAt = null;
            }
        });
    }

}

module.exports = Tasks;