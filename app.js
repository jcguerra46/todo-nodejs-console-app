require('colors');

const { inquirerMenu, 
    pause,
    readInput,
    listTasksToDelete,
    confirm,
    showCheckList
} = require('./helpers/inquirer');
const { index, 
    store 
} = require('./helpers/saveFile');
const Task = require('./models/task');
const Tasks = require('./models/tasks');

const main = async () => {

    let opt = '';
    const tasks = new Tasks();

    const readDB = index();
    if(readDB){
        tasks.insertTasksFronArray(readDB);
    }

    do {
        opt = await inquirerMenu();
        
        switch (opt) {
            case '1':
                // create task
                const desc = await readInput('Descripcion:');
                tasks.createTask( desc );
                break;

            case '2':
                // tasks.listArr
                tasks.completeList();
                break;
            
            case '3': 
                // Completadas
                tasks.completedTasks(true);
                break;

            case '4': 
                // Pendientes
                tasks.completedTasks(false);
                break;

            case '5':
                // completado | pendiente
                const ids = await showCheckList( tasks.listArr );
                tasks.toggleCompleted( ids );
                break;

            case '6':
                // Delete task
                // get Id
                const id = await listTasksToDelete( tasks.listArr );
                
                // confirm
                if ( id !== '0' ) {
                    const isConfirmed = await confirm('¿Are you sure?');

                    // delete task
                    if ( isConfirmed ) {
                        tasks.deleteTask(id);
                    }
                }
                break;
            
        }


       store( tasks.listArr );

        await pause();
        
    } while (opt !== '0');
}

main();