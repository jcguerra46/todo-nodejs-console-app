# README #

### TODO NodeJS Console APP ###
With this app you can create tasks and manage pending and completed tasks by console. 

## Steps
First that all, clone this repo and then install all dependencies
```sh
$ npm install
```

## Run the application
```sh
$ npm start
```

## Author
Juan Carlos Guerra 
```sh
email: juancarlosguerra46@gmail.com
```